import React,{Component} from "react";
import ReactDOM from "react-dom";

export default class Click extends Component{

    handleClick=(e) =>{
        this.setState((state,props)=>{
            // get current state and props by passing function into setState
            // if I dont bind this width this box, React will forget the this value
            return{
                clickCount:state.clickCount + 1
            }

        })
    }

    constructor(props){
        super(props);
        this.state={
            clickCount:0
        }
        // this.handleClick=this.handleClick.bind(this)   1
    }

    render(){
        return(
            <div>
                <p> click count is : {this.state.clickCount}</p>
                {/* <button onClick={this.handleClick}>Click me!</button>     1*/}
                {/* <button onClick={(e)=>this.handleClick(e)}>Click me!</button>    2*/}
                <button onClick={this.handleClick}>Click me!</button>
                <div>
                    <a href="https://kilid.com" onClick={this.linkClick}>link of kilid web site</a>
                </div>
            </div>
        )
    }

    handleClick(event){
        console.log(event);
        // this.setState((state,props)=>{
        //     // get current state and props by passing function into setState
        //     // it I dont bind this width this box, React will forget the this value
        //     return{
        //         clickCount:state.clickCount + 1
        //     }

        // })
    }

    linkClick(event){
        event.preventDefault();
        console.log('clicked');
        console.log(event);
        setTimeout(function () {console.log(event)},500)
        ;
        
    }
}
