import React,{Component} from "react";

export default class Clock extends Component{
    constructor(props){
        // we need father constructor for this perpus
        super(props); 
        this.state={
            date: new Date(),
        }
        setInterval(this.tick.bind(this),1000)
    }
    render(){
        return(
          <div>
              <p>Welcome New User!!!</p>
              <p>It is {this.state.date.toLocaleTimeString()}</p>
          </div>  
        )
    }
    // should be replaced by lifecycle in react
    // changes state to make react render our project again
    tick(){
         // setState comes from Componet ( which we extend) what is new
        this.setState({
            date: new Date(),
        })
        
        // we put an object which is itself (to render new time)
    }

}