import React from "react";
import TheNote from "./singlenote";

export default function Stickynots(){
    const noteData=[
        {title:'Teil number 1 ' ,content:'Text content number 1'},
        {title:'Teil number 2' ,content:'Text content number 2'},
        {title:'Teil number 3' ,content:'Text content number 3'},
        {title:'Teil number 4' ,content:'Text content number 4'},
        {title:'Teil number 5' ,content:'Text content number 5'},
        {title:'Teil number 6' ,content:'Text content number 6'},
        {title:'Teil number 7' ,content:'Text content number 7'},
    ]
    const renderNotes=noteData.map((eachNote,index) =><TheNote key={index} title={eachNote.title} content={eachNote.content} />)
    console.log(renderNotes);
    // list rendering 

    return( <ul>
                {renderNotes} 

                {/* <TheNote title="Teil number 1" content="Text content number 1" /> 
                <TheNote title="Teil number 2" content="Text content number 2" />
                <TheNote title="Teil number 3" content="Text content number 3" />
                <TheNote title="Teil number 4" content="Text content number 4" />
                <TheNote title="Teil number 5" content="Text content number 5" />
                <TheNote title="Teil number 6" content="Text content number 6" />
                <TheNote title="Teil number 7" content="Text content number 7" />   */}
            </ul>
    )

    // bist 15.10 ten zehnten Fideo ins meine lieblich Lektion
}