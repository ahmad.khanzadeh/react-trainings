import React from "react";

export default function TheNote(props){
    return(<li>
                <a href="#">
                    <h2>{props.title}</h2>
                    <p>{props.content}</p>
                </a>
            </li>
    )
}
